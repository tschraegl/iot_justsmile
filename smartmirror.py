# smartmirror.py
# requirements
# requests, feedparser, traceback, Pillow

from Tkinter import *
import locale
import threading
import time
import json
import traceback
import feedparser
import datetime

from PIL import Image, ImageTk
from contextlib import contextmanager

LOCALE_LOCK = threading.Lock()

# Visualization Settings
xlarge_text_size = 105
large_text_size = 55
medium_text_size = 40
small_text_size = 30

#Azure Parameter
account_key_blob = 'xxx'
subscription_key_blob = 'xx'
account_name_blob = 'xx'
blob_name = 'xx'
imageURL = 'xx'


@contextmanager
def setlocale(name): #thread proof function to work with locale
    with LOCALE_LOCK:
        saved = locale.setlocale(locale.LC_ALL)
        try:
            yield locale.setlocale(locale.LC_ALL, name)
        finally:
            locale.setlocale(locale.LC_ALL, saved)


globvar = 0
image_url = ""

class Mirror(Frame):
    
    
    
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, bg='black')
        # initialize time label
        self.time1 = ''
        self.timeLbl = Label(self, font=('Helvetica', large_text_size), fg="white", bg="black")
        self.timeLbl.pack(side=TOP, anchor=E)
        print("CurrentLabeltext: " + str(self.timeLbl))        
        self.tick()


    def tick(self):
        import time
        global globvar
        print("globvar: " + str(globvar))
        if globvar == 7:
            time1 = ""
            globvar = 8
            self.timeLbl.config(text=time1)
        if globvar == 6:
            time1 = "Goodbye."
            globvar = 7
            self.timeLbl.config(text=time1)
        if globvar == 5:
            time1 = "Thank you very much for your help. \n We wish you a nice day. "
            globvar = 6
            self.timeLbl.config(text=time1)
        if globvar == 4:
            emotion = getEmotion()
            time.sleep(2)
            
            if emotion == "happiness":
                time1 = "We are very pleased that you are happy with our service."
            elif (emotion == "neutral") or (emotion == "anger") or (emotion == "disgust") or (emotion == "sadness") :
                time1 = "We're sorry that you are not happy \n with our service. We'll continue working to \n improve hygiene for your next visit."
            globvar = 5
            self.timeLbl.config(text=time1)
        if globvar == 3.2:
            time1 = "3..2..1.."
            self.timeLbl.config(text=time1)        
            globvar = 4
        if globvar == 3.1:
            time1 = "3..2.."
            self.timeLbl.config(text=time1)        
            globvar = 3.2    
        if globvar == 3:
            time1 = "3.."
            self.timeLbl.config(text=time1)        
            globvar = 3.1
        if globvar == 2:
            time1 = "We will now take a photo to see \n if you are happy with our service.\n \n :) -> happy        :( -> not happy"
            globvar = 3
            self.timeLbl.config(text=time1)
        if globvar == 1:
            time1 = "Hi, nice to see you"
            globvar = 2
            self.timeLbl.config(text=time1)
        if globvar == 0:
            time1 = start()
            self.timeLbl.config(text=time1)
        
        
        if globvar == 0:
            self.timeLbl.after(20000, self.tick)
        if globvar == 1:
            self.timeLbl.after(3000, self.tick)
        if globvar == 2:
            self.timeLbl.after(3000, self.tick)
        if globvar == 3:
            self.timeLbl.after(6000, self.tick)
        if globvar == 3.1:
            self.timeLbl.after(2000, self.tick)
        if globvar == 3.2:
            self.timeLbl.after(2000, self.tick)
        if globvar == 4:
            self.timeLbl.after(2000, self.tick)
        if globvar == 5:
            self.timeLbl.after(4000, self.tick)
        if globvar == 6:
            self.timeLbl.after(3000, self.tick)
        if globvar == 7:
            self.timeLbl.after(3000, self.tick)
        if globvar == 8:
            globvar = 0
            self.timeLbl.after(60000, self.tick)
        
        
        
        #self.timeLbl.after(500, self.tick)
        
        


def getEmotion():
    import requests
    import datetime
    import os 
    global account_key_blob
    global subscription_key_blob
    global account_name_blob
    #take image
    os.system('fswebcam -r 1280x720 -S 20 --no-banner --jpeg 100 --save /home/pi/Pictures/to_transmit/image.jpeg')
    
    #define name of imae
    img_name = str(datetime.datetime.now().isoformat()) + ".jpeg"
    print("img_bname:")
    print(img_name)
    
    #upload image
    from azure.storage.blob import BlockBlobService
    
    block_blob_service = BlockBlobService(account_name=account_name_blob, account_key=account_key_blob)
    block_blob_service.create_blob_from_path(blob_name, img_name, '/home/pi/Pictures/to_transmit/image.jpeg')
    
    print("Image uploaded to Microsoft Azure")
    
    # Subscription key for Azure
    subscription_key = subscription_key_blob
    assert subscription_key

    # Request URL for cognitive service
    face_api_url = 'https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect'

    
    image_url = (imageURL + img_name)
   
    # Define parameters for Azure cognitive service
    headers = {'Ocp-Apim-Subscription-Key': subscription_key}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,' +
        'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise'
    }
    data = {'url': image_url}

    # Make HTTP Request and receive response
    response = requests.post(face_api_url, params=params, headers=headers, json=data)
    response = response.json()
    if response == []:
        return "No Person detected"
    
    print(response)
    # Extract information from response
    attributes = response[0]

    gender = attributes["faceAttributes"]["gender"]
    age = (attributes["faceAttributes"]["age"])
    glasses = attributes["faceAttributes"]["glasses"]
    anger = (attributes["faceAttributes"]["emotion"]["anger"])
    contempt = (attributes["faceAttributes"]["emotion"]["contempt"])
    disgust = (attributes["faceAttributes"]["emotion"]["disgust"])
    fear = (attributes["faceAttributes"]["emotion"]["fear"])
    happiness = (attributes["faceAttributes"]["emotion"]["happiness"])
    neutral = (attributes["faceAttributes"]["emotion"]["neutral"])
    sadness = (attributes["faceAttributes"]["emotion"]["sadness"])
    surprise = (attributes["faceAttributes"]["emotion"]["surprise"])
    
           
    allEmotions = [anger, contempt, disgust, fear, happiness, neutral, sadness, surprise]
    print("allEmotions:")
    print(allEmotions)
    
    #find max
    max_value = max(allEmotions)
    print('max_value:')
    print(max_value)
    position = allEmotions.index(max_value)
    print('position:')
    print(position)
    
    emotion = ""
    if position == 0:
        emotion = "anger"
    else:
        pass
    if position == 1:
        emotion = "contempt"
    else:
        pass
    if position == 2:
        emotion = "disgust"
    else:
        pass
    if position == 3:
        emotion = "fear"
    else:
        pass
    if position == 4:
        emotion = "happiness"
    else:
        pass
    if position == 5:
        emotion = "neutral"
    else:
        pass
    if position == 6:
        emotion = "sadness"
    else:
        pass
    if position == 7:
        emotion = "surprise"
    else:
        pass
    
    print('Emotion:')
    print(emotion)


    #write results to csv and save it to the home directory of our apache webservice
    import csv
    with open('/var/www/html/data.csv', mode='a') as results_file:
        data_writer = csv.writer(results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow([str(datetime.datetime.now().isoformat()), gender, age, glasses, emotion])
        
    #delete Image
    block_blob_service.delete_blob(blob_name, img_name)
    
    return emotion





def percent_cb(complete, total):
    sys.stdout.write('.')
    sys.stdout.flush()


    
def start():
    import requests
    import datetime
    import os 
    #import boto
    #import boto.s3
    #from boto.s3.key import Key
    from PIL import Image


    global globvar
    global image_url
    global account_key_blob
    global account_name_blob
    # Save file locally on device
    os.system('fswebcam -r 1280x720 -S 20 --no-banner --jpeg 100 --save /home/pi/Pictures/to_transmit/image.jpeg')
    
    #define name of imae
    img_name = str(datetime.datetime.now().isoformat()) + ".jpeg"
    print("img_bname:")
    print(img_name)

    # Upload image to Azure Block
    from azure.storage.blob import BlockBlobService
    
    block_blob_service = BlockBlobService(account_name=account_name_blob, account_key=account_key_blob)
    block_blob_service.create_blob_from_path(blob_name, img_name, '/home/pi/Pictures/to_transmit/image.jpeg')
    print("Image uploaded to Microsoft Azure")
    
    # Subscription key for Azure
    subscription_key = subscription_key_blob
    assert subscription_key

    # Request URL for cognitive service
    face_api_url = 'https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect'

    # Set image_url to the URL of an image that you want to analyze.
    image_url = (imageURL + img_name)

    # Define parameters for Azure cognitive service
    headers = {
        'Content-Type':'application/json',
        'Ocp-Apim-Subscription-Key': subscription_key
               }
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,' +
        'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise'
    }
    data = {'url': image_url}    

    # Make HTTP Request and receive response
    r = requests.post(face_api_url, params=params, headers=headers, json=data)
    print(r.status_code, r.reason)
    data1 = r.json()
    print(data1)
   
    block_blob_service.delete_blob(blob_name, img_name)

    if data1 == []:
        print("No Person detected")
        globvar = 0
        return ""
    else:
        print("Person detected. Start Feedback loop")
        globvar = 1
        return ""


class FullscreenWindow:

    def __init__(self):
        self.tk = Tk()
        self.tk.configure(background='black')
        self.topFrame = Frame(width=200, height=50, background = 'black')
        self.bottomFrame = Frame(self.tk, background = 'black')
        self.topFrame.pack(side = TOP, fill=BOTH, expand = YES)
        self.bottomFrame.pack(side = BOTTOM, fill=BOTH, expand = YES)
        self.state = False
        # Mirror
        self.mirror = Mirror(self.topFrame)
        self.mirror.pack(side=LEFT, anchor=N, padx=100, pady=60)
        self.tk.attributes('-fullscreen', True)
        

    def toggle_fullscreen(self, event=None):
        self.state = not self.state  # Just toggling the boolean
        self.tk.attributes("-fullscreen", self.state)
        return "break"

    def end_fullscreen(self, event=None):
        self.state = False
        self.tk.attributes("-fullscreen", False)
        return "break"

if __name__ == '__main__':
    w = FullscreenWindow()
    w.tk.mainloop()
